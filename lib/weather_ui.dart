import 'package:flutter/material.dart';
import 'dart:core';
import 'dart:async';

void main() {
  runApp(WeatherApp());
}

class WeatherApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Weather App',
      home: WeatherScreen(),
    );
  }
}

class WeatherScreen extends StatefulWidget {
  @override
  _WeatherScreenState createState() => _WeatherScreenState();
}

class _WeatherScreenState extends State<WeatherScreen> {
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/bg.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              alignment: Alignment(0, -0.9),
              child: LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  double fontSize = constraints.maxHeight / 5 < constraints.maxWidth / 3? constraints.maxHeight / 10 : constraints.maxWidth / 6;
                  return Text(
                    '${getDayOfWeekString(DateTime.now().weekday)}',
                    style: TextStyle(
                      fontSize: fontSize,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  );
                },
              ),
            ),
            Container(
              alignment: Alignment(0, -0.5),
              child: LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  double fontSize = (constraints.maxHeight / 5 < constraints.maxWidth / 3? constraints.maxHeight / 5 : constraints.maxWidth / 3)+5;
                  return Text(
                    '35°C',
                    style: TextStyle(
                      fontSize: fontSize,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  );
                },
              ),
            ),
            Container(
              alignment: Alignment(0, 0),
              child: LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  double fontSize = constraints.maxHeight / 5 < constraints.maxWidth / 3? constraints.maxHeight / 10 : constraints.maxWidth / 6;
                  return Text(
                    'Chonburi',
                    style: TextStyle(
                      fontSize: fontSize,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  );
                },
              ),
            ),
            Container(
              alignment: Alignment(0, 0.4),
              child: LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  double iconSize = constraints.maxHeight / 5 < constraints.maxWidth / 3? constraints.maxHeight / 5 : constraints.maxWidth / 3;
                  return Icon(
                    Icons.sunny,
                    size: iconSize,
                    color: Colors.white,
                  );
                },
              ),
            ),
            Container(
              alignment: Alignment(0, 0.8),
              child: LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  double fontSize = constraints.maxHeight / 5 < constraints.maxWidth / 3? constraints.maxHeight / 10 : constraints.maxWidth / 6;
                  return Text(
                    '${DateTime.now().hour}:${DateTime.now().minute}:${DateTime.now().second}',
                    style: TextStyle(
                      fontSize: fontSize,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  );
                },
              ),
            ),
          ],
        )
    );
  }
}

String getDayOfWeekString(int dayOfWeek) {
  switch (dayOfWeek) {
    case 1:
      return 'Monday';
    case 2:
      return 'Tuesday';
    case 3:
      return 'Wednesday';
    case 4:
      return 'Thursday';
    case 5:
      return 'Friday';
    case 6:
      return 'Saturday';
    case 7:
      return 'Sunday';
    default:
      return 'Unknown';
  }
}